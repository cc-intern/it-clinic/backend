import { Order } from 'src/orders/entities/order.entity';
import { Question } from 'src/questions/entities/question.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class OrderRating {
  @PrimaryGeneratedColumn()
  order_rating_id: number;

  @Column()
  order_rating_question: string;

  @Column()
  order_rating_rating: number;

  @ManyToOne(() => Question, (question) => question.order_ratings)
  question: Question;

  @ManyToOne(() => Order, (order) => order.order_ratings)
  order: Order;

  @CreateDateColumn()
  order_rating_createdDate: Date;
}
