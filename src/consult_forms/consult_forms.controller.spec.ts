import { Test, TestingModule } from '@nestjs/testing';
import { ConsultFormsController } from './consult_forms.controller';
import { ConsultFormsService } from './consult_forms.service';

describe('ConsultFormsController', () => {
  let controller: ConsultFormsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ConsultFormsController],
      providers: [ConsultFormsService],
    }).compile();

    controller = module.get<ConsultFormsController>(ConsultFormsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
