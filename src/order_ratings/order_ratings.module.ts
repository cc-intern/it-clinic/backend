import { Module } from '@nestjs/common';
import { OrderRatingsService } from './order_ratings.service';
import { OrderRatingsController } from './order_ratings.controller';

@Module({
  controllers: [OrderRatingsController],
  providers: [OrderRatingsService],
})
export class OrderRatingsModule {}
