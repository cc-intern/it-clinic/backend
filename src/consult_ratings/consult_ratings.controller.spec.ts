import { Test, TestingModule } from '@nestjs/testing';
import { ConsultRatingsController } from './consult_ratings.controller';
import { ConsultRatingsService } from './consult_ratings.service';

describe('ConsultRatingsController', () => {
  let controller: ConsultRatingsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ConsultRatingsController],
      providers: [ConsultRatingsService],
    }).compile();

    controller = module.get<ConsultRatingsController>(ConsultRatingsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
