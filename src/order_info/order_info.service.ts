import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from 'src/orders/entities/order.entity';
import { Repository } from 'typeorm';
import { CreateOrderInfoDto } from './dto/create-order_info.dto';
import { UpdateOrderInfoDto } from './dto/update-order_info.dto';
import { OrderInfo } from './entities/order_info.entity';

@Injectable()
export class OrderInfoService {
  constructor(
    @InjectRepository(OrderInfo)
    private orderInfoRepository: Repository<OrderInfo>,
  ) {}
  create(createOrderInfoDto: CreateOrderInfoDto) {
    return this.orderInfoRepository.save(createOrderInfoDto);
  }

  findAll() {
    return this.orderInfoRepository.find();
  }

  findOne(id: number) {
    return this.orderInfoRepository.findOne({
      where: { order_info_id: id },
    });
  }

  update(id: number, updateOrderInfoDto: UpdateOrderInfoDto) {
    return `This action updates a #${id} orderInfo`;
  }

  remove(id: number) {
    return `This action removes a #${id} orderInfo`;
  }
}
