import { Module } from '@nestjs/common';
import { OrderInfoService } from './order_info.service';
import { OrderInfoController } from './order_info.controller';
import { OrderInfo } from './entities/order_info.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([OrderInfo])],
  controllers: [OrderInfoController],
  providers: [OrderInfoService],
})
export class OrderInfoModule {}
