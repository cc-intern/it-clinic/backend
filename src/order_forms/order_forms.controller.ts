import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { OrderFormsService } from './order_forms.service';
import { CreateOrderFormDto } from './dto/create-order_form.dto';
import { UpdateOrderFormDto } from './dto/update-order_form.dto';

@Controller('order-forms')
export class OrderFormsController {
  constructor(private readonly orderFormsService: OrderFormsService) {}

  @Post()
  create(@Body() createOrderFormDto: CreateOrderFormDto) {
    return this.orderFormsService.create(createOrderFormDto);
  }

  @Get()
  findAll() {
    return this.orderFormsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.orderFormsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateOrderFormDto: UpdateOrderFormDto,
  ) {
    return this.orderFormsService.update(+id, updateOrderFormDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.orderFormsService.remove(+id);
  }
}
