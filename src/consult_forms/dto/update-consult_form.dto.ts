import { PartialType } from '@nestjs/mapped-types';
import { CreateConsultFormDto } from './create-consult_form.dto';

export class UpdateConsultFormDto extends PartialType(CreateConsultFormDto) {}
