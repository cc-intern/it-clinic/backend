import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ConsultFormsService } from './consult_forms.service';
import { CreateConsultFormDto } from './dto/create-consult_form.dto';
import { UpdateConsultFormDto } from './dto/update-consult_form.dto';

@Controller('consult-forms')
export class ConsultFormsController {
  constructor(private readonly consultFormsService: ConsultFormsService) {}

  @Post()
  create(@Body() createConsultFormDto: CreateConsultFormDto) {
    return this.consultFormsService.create(createConsultFormDto);
  }

  @Get()
  findAll() {
    return this.consultFormsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.consultFormsService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateConsultFormDto: UpdateConsultFormDto) {
    return this.consultFormsService.update(+id, updateConsultFormDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.consultFormsService.remove(+id);
  }
}
