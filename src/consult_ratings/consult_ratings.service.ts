import { Injectable } from '@nestjs/common';
import { CreateConsultRatingDto } from './dto/create-consult_rating.dto';
import { UpdateConsultRatingDto } from './dto/update-consult_rating.dto';

@Injectable()
export class ConsultRatingsService {
  create(createConsultRatingDto: CreateConsultRatingDto) {
    return 'This action adds a new consultRating';
  }

  findAll() {
    return `This action returns all consultRatings`;
  }

  findOne(id: number) {
    return `This action returns a #${id} consultRating`;
  }

  update(id: number, updateConsultRatingDto: UpdateConsultRatingDto) {
    return `This action updates a #${id} consultRating`;
  }

  remove(id: number) {
    return `This action removes a #${id} consultRating`;
  }
}
