import { IsNotEmpty } from 'class-validator';

export class CreateStaffDto {
  @IsNotEmpty()
  staff_username: string;

  @IsNotEmpty()
  staff_role: string;
}
