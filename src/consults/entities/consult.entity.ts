import { ConsultForm } from 'src/consult_forms/entities/consult_form.entity';
import { ConsultRating } from 'src/consult_ratings/entities/consult_rating.entity';

import { Staff } from 'src/staffs/entities/staff.entity';

import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Consult {
  @PrimaryGeneratedColumn()
  consult_id: number;

  @Column()
  consult_username: string;

  @Column()
  consult_fullname: string;

  @Column()
  consult_email: string;

  @Column()
  consult_type: string;

  @Column()
  consult_question: string;

  @Column({ default: null })
  consult_answer: string;

  @Column({ default: 'รอดำเนินการ' })
  consult_status: string;

  @CreateDateColumn()
  consult_createdDate: Date;

  @UpdateDateColumn()
  consult_updatedDate: Date;

  @DeleteDateColumn()
  consult_deletedDate: Date;

  // @ManyToOne(() => Form, (form) => form.consults)
  // form: Form;

  @OneToMany(() => ConsultRating, (consult_rating) => consult_rating.consult)
  consult_ratings: ConsultRating[];

  @ManyToOne(() => Staff, (staff) => staff.consults)
  staff: Staff;

  @OneToMany(() => ConsultForm, (consult_form) => consult_form.consult)
  consult_forms: ConsultForm[];
}
