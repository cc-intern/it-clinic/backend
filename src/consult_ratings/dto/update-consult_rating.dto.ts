import { PartialType } from '@nestjs/mapped-types';
import { CreateConsultRatingDto } from './create-consult_rating.dto';

export class UpdateConsultRatingDto extends PartialType(
  CreateConsultRatingDto,
) {}
