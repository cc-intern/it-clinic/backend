import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateInformationDto } from './dto/create-information.dto';
import { UpdateInformationDto } from './dto/update-information.dto';
import { Information } from './entities/information.entity';

@Injectable()
export class InformationsService {
  constructor(
    @InjectRepository(Information)
    private informationRepository: Repository<Information>,
  ) {}
  create(createInformationDto: CreateInformationDto) {
    return this.informationRepository.save(createInformationDto);
  }

  findAll() {
    return this.informationRepository.find();
  }

  findOne(id: number) {
    return this.informationRepository.findOne({
      where: { info_id: id },
    });
  }

  async update(id: number, updateInformationDto: UpdateInformationDto) {
    const info = await this.informationRepository.findOne({
      where: { info_id: id },
    });

    if (!info) {
      throw new NotFoundException();
    }

    const updateInfo = {
      ...info,
      ...updateInformationDto,
    };
    return this.informationRepository.save(updateInfo);
  }

  async remove(id: number) {
    const info = await this.informationRepository.findOne({
      where: { info_id: id },
    });

    if (!info) {
      throw new NotFoundException();
    }

    return this.informationRepository.remove(info);
  }
}
