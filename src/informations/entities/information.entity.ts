import { OrderInfo } from 'src/order_info/entities/order_info.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Information {
  @PrimaryGeneratedColumn()
  info_id: number;

  @Column()
  info_content: string;

  @OneToMany(() => OrderInfo, (order_info) => order_info.order)
  order_info: OrderInfo[];
}
