import { Module } from '@nestjs/common';
import { OrderFormsService } from './order_forms.service';
import { OrderFormsController } from './order_forms.controller';

@Module({
  controllers: [OrderFormsController],
  providers: [OrderFormsService],
})
export class OrderFormsModule {}
