import { ConsultRating } from 'src/consult_ratings/entities/consult_rating.entity';
import { Form } from 'src/forms/entities/form.entity';
import { OrderRating } from 'src/order_ratings/entities/order_rating.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Question {
  @PrimaryGeneratedColumn()
  ques_id: number;

  @Column()
  ques_question: string;

  @Column({ type: 'decimal', precision: 2, scale: 1, default: 0 })
  ques_avgRating: number;

  @ManyToOne(() => Form, (form) => form.questions)
  form: Form;

  @OneToMany(() => OrderRating, (order_rating) => order_rating.question)
  order_ratings: OrderRating[];

  @OneToMany(() => ConsultRating, (consult_rating) => consult_rating)
  consult_ratings: ConsultRating[];
}
