import { Consult } from 'src/consults/entities/consult.entity';
import { ConsultForm } from 'src/consult_forms/entities/consult_form.entity';

import { OrderForm } from 'src/order_forms/entities/order_form.entity';
import { Question } from 'src/questions/entities/question.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Form {
  @PrimaryGeneratedColumn()
  form_id: number;

  @Column()
  form_type: string;

  @Column()
  form_name: string;

  @Column({ type: 'decimal', precision: 2, scale: 1, default: 0 })
  form_avgRating: number;

  @OneToMany(() => Question, (question) => question.form)
  questions: Question[];

  @OneToMany(() => OrderForm, (order_form) => order_form.form)
  order_forms: OrderForm[];

  @OneToMany(() => ConsultForm, (consult_form) => consult_form.consult)
  consult_forms: ConsultForm[];
}
