import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import axios from 'axios';
import { Staff } from 'src/staffs/entities/staff.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AuthService {
  private token: string; // Add a private property to store the token

  constructor(
    @InjectRepository(Staff)
    private staffRepository: Repository<Staff>,

    private jwtService: JwtService,
  ) {}

  async logIn(loginObj: any) {
    const username: string = loginObj.username;
    const password: string = loginObj.password;

    // If the token doesn't exist or has expired, request a new token
    if (!this.token) {
      await this.requestNewToken();
    }

    const loginApi = 'https://service-api-dev.buu.ac.th/api/v1/loginBuu';
    try {
      const response = await axios.post(
        loginApi,
        { username: username, password: password, secret: false },
        {
          headers: {
            Accept: 'application/json',
            Authorization: 'Bearer ' + this.token,
          },
        },
      );
      console.log(response.data);
      const staff = await this.staffRepository.findOne({
        where: { staff_username: username },
      });

      const role = staff ? staff.staff_role : 'user';

      const user = {
        username: username,
        fullname: response.data.result.fullname,
        email: response.data.result.email,
        role: role,
      };

      return {
        user,
        access_token: await this.jwtService.signAsync(user),
      };
    } catch (error) {
      if (error.response.status === 401) {
        // If the API responds with 401 Unauthorized, the token may have expired
        await this.requestNewToken(); // Request a new token
        throw new UnauthorizedException();
      }
      throw error;
    }
  }

  private async requestNewToken() {
    const requrestTokenApi =
      'https://service-api-dev.buu.ac.th/api/loginRequestForToken';
    const user_login = 'itclinic';
    const user_password = 'itclinic@2023';
    const client_id = '6459c86f7eb90';
    const secret =
      '7c2fb99246c4baecaab6aeba20f295c5cf9fd32f1234744b78dda54ea1739f95';

    try {
      const response = await axios.post(requrestTokenApi, {
        user_login: user_login,
        user_password: user_password,
        client_id: client_id,
        secret: secret,
      });

      this.token = response.data.token; // Store the new token
      console.log(this.token);
    } catch (error) {
      throw new UnauthorizedException();
    }
  }
}
