import { Module } from '@nestjs/common';
import { ConsultRatingsService } from './consult_ratings.service';
import { ConsultRatingsController } from './consult_ratings.controller';

@Module({
  controllers: [ConsultRatingsController],
  providers: [ConsultRatingsService],
})
export class ConsultRatingsModule {}
