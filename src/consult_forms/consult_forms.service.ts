import { Injectable } from '@nestjs/common';
import { CreateConsultFormDto } from './dto/create-consult_form.dto';
import { UpdateConsultFormDto } from './dto/update-consult_form.dto';

@Injectable()
export class ConsultFormsService {
  create(createConsultFormDto: CreateConsultFormDto) {
    return 'This action adds a new consultForm';
  }

  findAll() {
    return `This action returns all consultForms`;
  }

  findOne(id: number) {
    return `This action returns a #${id} consultForm`;
  }

  update(id: number, updateConsultFormDto: UpdateConsultFormDto) {
    return `This action updates a #${id} consultForm`;
  }

  remove(id: number) {
    return `This action removes a #${id} consultForm`;
  }
}
