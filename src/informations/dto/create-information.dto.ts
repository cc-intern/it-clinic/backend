import { IsNotEmpty } from 'class-validator';

export class CreateInformationDto {
  @IsNotEmpty()
  info_content: string;
}
