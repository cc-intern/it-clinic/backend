import { PartialType } from '@nestjs/mapped-types';
import { CreateOrderInfoDto } from './create-order_info.dto';

export class UpdateOrderInfoDto extends PartialType(CreateOrderInfoDto) {}
