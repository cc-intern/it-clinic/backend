import { Consult } from 'src/consults/entities/consult.entity';
import { Form } from 'src/forms/entities/form.entity';

import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ConsultForm {
  @PrimaryGeneratedColumn()
  consult_form_id: number;

  @Column()
  consult_form_status: string;

  @ManyToOne(() => Consult, (consult) => consult.consult_forms)
  consult: Consult;

  @ManyToOne(() => Form, (form) => form.consult_forms)
  form: Form;
}
