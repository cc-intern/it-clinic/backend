import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateOrderProcessDto } from './dto/create-order_process.dto';
import { UpdateOrderProcessDto } from './dto/update-order_process.dto';
import { OrderProcess } from './entities/order_process.entity';

@Injectable()
export class OrderProcessService {
  constructor(
    @InjectRepository(OrderProcess)
    private orderProcessRepository: Repository<OrderProcess>,
  ) {}
  create(createOrderProcessDto: CreateOrderProcessDto) {
    return this.orderProcessRepository.save(createOrderProcessDto);
  }

  findAll() {
    return this.orderProcessRepository.find();
  }

  findOne(id: number) {
    return this.orderProcessRepository.findOne({
      where: { order_process_id: id },
    });
  }

  async update(id: number, updateOrderProcessDto: UpdateOrderProcessDto) {
    const order_process = await this.orderProcessRepository.findOne({
      where: { order_process_id: id },
    });

    if (!order_process) {
      throw new NotFoundException();
    }

    const updateOrderProcess = {
      ...order_process,
      ...updateOrderProcessDto,
    };
    return this.orderProcessRepository.save(updateOrderProcess);
  }

  async remove(id: number) {
    const order_process = await this.orderProcessRepository.findOne({
      where: { order_process_id: id },
    });

    if (!order_process) {
      throw new NotFoundException();
    }
    return this.orderProcessRepository.remove(order_process);
  }
}
