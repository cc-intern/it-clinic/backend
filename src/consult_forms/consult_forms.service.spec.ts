import { Test, TestingModule } from '@nestjs/testing';
import { ConsultFormsService } from './consult_forms.service';

describe('ConsultFormsService', () => {
  let service: ConsultFormsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ConsultFormsService],
    }).compile();

    service = module.get<ConsultFormsService>(ConsultFormsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
