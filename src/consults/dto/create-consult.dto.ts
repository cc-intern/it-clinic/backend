import { IsNotEmpty } from 'class-validator';

export class CreateConsultDto {
  @IsNotEmpty()
  consult_username: string;

  @IsNotEmpty()
  consult_fullname: string;

  @IsNotEmpty()
  consult_email: string;

  @IsNotEmpty()
  consult_type: string;

  @IsNotEmpty()
  consult_question: string;
}
