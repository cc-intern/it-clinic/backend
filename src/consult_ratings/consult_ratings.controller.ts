import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ConsultRatingsService } from './consult_ratings.service';
import { CreateConsultRatingDto } from './dto/create-consult_rating.dto';
import { UpdateConsultRatingDto } from './dto/update-consult_rating.dto';

@Controller('consult-ratings')
export class ConsultRatingsController {
  constructor(private readonly consultRatingsService: ConsultRatingsService) {}

  @Post()
  create(@Body() createConsultRatingDto: CreateConsultRatingDto) {
    return this.consultRatingsService.create(createConsultRatingDto);
  }

  @Get()
  findAll() {
    return this.consultRatingsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.consultRatingsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateConsultRatingDto: UpdateConsultRatingDto,
  ) {
    return this.consultRatingsService.update(+id, updateConsultRatingDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.consultRatingsService.remove(+id);
  }
}
