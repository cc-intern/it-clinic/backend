import { Consult } from 'src/consults/entities/consult.entity';
import { Order } from 'src/orders/entities/order.entity';
import { OrderProcess } from 'src/order_process/entities/order_process.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Staff {
  @PrimaryGeneratedColumn()
  staff_id: number;

  @Column()
  staff_username: string;

  @Column()
  staff_role: string;

  // @OneToMany(() => Order, (order) => order.staff)
  // orders: Order[];

  @OneToMany(() => Consult, (consult) => consult.staff)
  consults: Consult[];

  @OneToMany(() => OrderProcess, (order_process) => order_process.staff)
  orders_process: OrderProcess[];
}
