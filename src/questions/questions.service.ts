import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateQuestionDto } from './dto/create-question.dto';
import { UpdateQuestionDto } from './dto/update-question.dto';
import { Question } from './entities/question.entity';

@Injectable()
export class QuestionsService {
  constructor(
    @InjectRepository(Question)
    private questionRepository: Repository<Question>,
  ) {}
  create(createQuestionDto: CreateQuestionDto) {
    return this.questionRepository.save(createQuestionDto);
  }

  findAll() {
    return this.questionRepository.find({
      relations: ['form'],
    });
  }

  findOne(id: number) {
    return this.questionRepository.findOne({
      where: { ques_id: id },
      relations: ['form'],
    });
  }

  async update(id: number, updateQuestionDto: UpdateQuestionDto) {
    const question = await this.questionRepository.findOne({
      where: { ques_id: id },
    });
    if (!question) {
      throw new NotFoundException();
    }

    const updateQuestion = {
      ...question,
      ...updateQuestionDto,
    };
    return this.questionRepository.save(updateQuestion);
  }

  async remove(id: number) {
    const question = await this.questionRepository.findOne({
      where: { ques_id: id },
    });
    if (!question) {
      throw new NotFoundException();
    }
    return this.questionRepository.remove(question);
  }
}
