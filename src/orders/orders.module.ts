import { Module } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { Form } from 'src/forms/entities/form.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './entities/order.entity';
import { OrderForm } from 'src/order_forms/entities/order_form.entity';
import { Information } from 'src/informations/entities/information.entity';
import { OrderInfo } from 'src/order_info/entities/order_info.entity';
import { Staff } from 'src/staffs/entities/staff.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Form,
      Order,
      OrderForm,
      Information,
      OrderInfo,
      Staff,
    ]),
  ],
  controllers: [OrdersController],
  providers: [OrdersService],
})
export class OrdersModule {}
