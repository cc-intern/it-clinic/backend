import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Order } from './orders/entities/order.entity';
import { OrdersModule } from './orders/orders.module';
import { ConsultsModule } from './consults/consults.module';
import { Consult } from './consults/entities/consult.entity';
import { QuestionsModule } from './questions/questions.module';
import { FormsModule } from './forms/forms.module';
import { Question } from './questions/entities/question.entity';
import { Form } from './forms/entities/form.entity';
import { OrderRatingsModule } from './order_ratings/order_ratings.module';
import { ConsultRatingsModule } from './consult_ratings/consult_ratings.module';
import { OrderRating } from './order_ratings/entities/order_rating.entity';
import { ConsultRating } from './consult_ratings/entities/consult_rating.entity';
import { OrderProcessModule } from './order_process/order_process.module';
import { OrderProcess } from './order_process/entities/order_process.entity';
import { OrderFormsModule } from './order_forms/order_forms.module';
import { OrderForm } from './order_forms/entities/order_form.entity';
import { InformationsModule } from './informations/informations.module';
import { OrderInfoModule } from './order_info/order_info.module';
import { Information } from './informations/entities/information.entity';
import { OrderInfo } from './order_info/entities/order_info.entity';
import { StaffsModule } from './staffs/staffs.module';
import { Staff } from './staffs/entities/staff.entity';
import { AuthModule } from './auth/auth.module';
import { ConsultFormsModule } from './consult_forms/consult_forms.module';
import { ConsultForm } from './consult_forms/entities/consult_form.entity';
import { DataSource } from 'typeorm';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      // type: 'mysql',
      // host: '10.5.1.92',
      // port: 3306,
      // username: 'it-clinic',
      // password: 'aGT4AaZpqIRb0dDO',
      // database: 'it-clinic',
      type: 'sqlite',
      database: 'it-clinic.sqlite',
      synchronize: true,
      entities: [
        Order,
        Consult,
        Question,
        Form,
        OrderRating,
        ConsultRating,
        OrderProcess,
        OrderForm,
        Information,
        OrderInfo,
        Staff,
        ConsultForm,
      ],
    }),
    OrdersModule,
    ConsultsModule,
    QuestionsModule,
    FormsModule,
    OrderRatingsModule,
    ConsultRatingsModule,
    OrderProcessModule,
    OrderFormsModule,
    InformationsModule,
    OrderInfoModule,
    StaffsModule,
    AuthModule,
    ConsultFormsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
