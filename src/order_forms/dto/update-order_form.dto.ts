import { PartialType } from '@nestjs/mapped-types';
import { CreateOrderFormDto } from './create-order_form.dto';

export class UpdateOrderFormDto extends PartialType(CreateOrderFormDto) {}
