import { OrderForm } from 'src/order_forms/entities/order_form.entity';
import { OrderInfo } from 'src/order_info/entities/order_info.entity';
import { OrderProcess } from 'src/order_process/entities/order_process.entity';
import { OrderRating } from 'src/order_ratings/entities/order_rating.entity';
import { Staff } from 'src/staffs/entities/staff.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  order_id: number;

  @Column()
  order_reserveDate: Date;

  @Column()
  order_username: string;

  @Column()
  order_fullname: string;

  @Column()
  order_email: string;

  // @Column()
  // order_tel: string;

  // @Column({ nullable: true })
  // order_lineId: string;

  // @Column()
  // order_deviceType: string;

  // @Column({ nullable: true })
  // order_deviceModel: string;

  // @Column({ nullable: true })
  // order_deviceSerial: string;

  // @Column({ nullable: true })
  // order_deviceSpec: string;

  // @Column({ nullable: true })
  // order_devicePassword: string;

  // @Column()
  // order_serviceType: string;

  // @Column()
  // order_serviceDetail: string;

  // @Column()
  // order_serviceStatus: string;

  // @Column({ nullable: true })
  // order_serviceNote: string;

  // @Column()
  // order_receiverEmail: string;

  @CreateDateColumn()
  order_createdDate: Date;

  @UpdateDateColumn()
  order_updatedDate: Date;

  @DeleteDateColumn()
  order_deletedDate: Date;

  @OneToMany(() => OrderRating, (order_rating) => order_rating.order)
  order_ratings: OrderRating[];

  @OneToMany(() => OrderProcess, (order_process) => order_process.order)
  order_process: OrderProcess[];

  @OneToMany(() => OrderForm, (order_form) => order_form.order)
  order_forms: OrderForm[];

  // @ManyToOne(() => Staff, (staff) => staff.orders)
  // staff: Staff;

  @OneToMany(() => OrderInfo, (order_info) => order_info.order)
  order_info: OrderInfo[];
}
