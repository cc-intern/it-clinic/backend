import { Order } from 'src/orders/entities/order.entity';
import { Staff } from 'src/staffs/entities/staff.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class OrderProcess {
  @PrimaryGeneratedColumn()
  order_process_id: number;

  @Column()
  order_process_content: string;

  @CreateDateColumn()
  order_process_createdDate: Date;

  @UpdateDateColumn()
  order_process_updatedDate: Date;

  @DeleteDateColumn()
  order_process_deletedDate: Date;

  @ManyToOne(() => Order, (order) => order.order_process)
  order: Order;

  @ManyToOne(() => Staff, (staff) => staff.orders_process)
  staff: Staff;
}
