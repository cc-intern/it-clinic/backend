import { Consult } from 'src/consults/entities/consult.entity';
import { Question } from 'src/questions/entities/question.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class ConsultRating {
  @PrimaryGeneratedColumn()
  consult_rating_id: number;

  @Column()
  consult_rating_question: string;

  @Column()
  consult_rating_rating: number;

  @ManyToOne(() => Question, (question) => question.consult_ratings)
  question: Question;

  @ManyToOne(() => Consult, (consult) => consult.consult_ratings)
  consult: Consult;

  @CreateDateColumn()
  consult_rating_createdDate: Date;
}
