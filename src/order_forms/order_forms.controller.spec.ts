import { Test, TestingModule } from '@nestjs/testing';
import { OrderFormsController } from './order_forms.controller';
import { OrderFormsService } from './order_forms.service';

describe('OrderFormsController', () => {
  let controller: OrderFormsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OrderFormsController],
      providers: [OrderFormsService],
    }).compile();

    controller = module.get<OrderFormsController>(OrderFormsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
