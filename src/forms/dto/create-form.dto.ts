import { IsNotEmpty } from 'class-validator';

export class CreateFormDto {
  @IsNotEmpty()
  form_type: string;

  @IsNotEmpty()
  form_name: string;
}
