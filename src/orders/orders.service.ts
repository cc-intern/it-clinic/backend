import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Information } from 'src/informations/entities/information.entity';
import { OrderInfo } from 'src/order_info/entities/order_info.entity';
import { Staff } from 'src/staffs/entities/staff.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Order } from './entities/order.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private orderRepository: Repository<Order>,
    @InjectRepository(Staff)
    private staffRepository: Repository<Staff>,
    @InjectRepository(OrderInfo)
    private orderInfoRepository: Repository<OrderInfo>,
    @InjectRepository(Information)
    private informationRepository: Repository<Information>,
  ) {}
  async create(createOrderDto: CreateOrderDto) {
    const order: Order = new Order();
    order.order_reserveDate = createOrderDto.order_reserveDate;
    order.order_username = createOrderDto.order_username;
    await this.orderRepository.save(order);
    for (const rec of createOrderDto.orderInfo) {
      const order_info = new OrderInfo();
      order_info.infomation = await this.informationRepository.findOne({
        where: { info_id: rec.infoId },
      });
      order_info.order = await this.orderRepository.findOne({
        where: { order_id: rec.orderId },
      });
      order_info.order_info_answer = rec.order_info_answer;
      order_info.order = rec.order;
      order_info.orderId = rec.orderId;
      order_info.infoId = rec.infoId;

      await this.orderInfoRepository.save(order_info);
    }
    await this.orderRepository.save(order);
    return this.orderRepository.findOneOrFail({
      where: { order_id: order.order_id },
      relations: ['order_info'],
    });
  }

  findAll() {
    return this.orderRepository.find();
  }

  findOne(id: number) {
    return this.orderRepository.findOne({
      where: { order_id: id },
    });
  }

  async update(id: number, updateOrderDto: UpdateOrderDto) {
    const order = await this.orderRepository.findOne({
      where: { order_id: id },
    });

    if (!order) {
      throw new NotFoundException();
    }

    const updateOrder = {
      ...order,
      ...updateOrderDto,
    };
    return this.orderRepository.save(updateOrder);
  }

  async remove(id: number) {
    const order = await this.orderRepository.findOne({
      where: { order_id: id },
    });

    if (!order) {
      throw new NotFoundException();
    }
    return this.orderRepository.softRemove(order);
  }
}
