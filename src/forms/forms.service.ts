import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CreateQuestionDto } from 'src/questions/dto/create-question.dto';
import { Question } from 'src/questions/entities/question.entity';

import { Repository } from 'typeorm';
import { CreateFormDto } from './dto/create-form.dto';
import { UpdateFormDto } from './dto/update-form.dto';
import { Form } from './entities/form.entity';

@Injectable()
export class FormsService {
  constructor(
    @InjectRepository(Form)
    private formRepository: Repository<Form>,
    @InjectRepository(Question)
    private questionRepository: Repository<Question>,
  ) {}
  create(createFormDto: CreateFormDto) {
    return this.formRepository.save(createFormDto);
  }

  findAll() {
    return this.formRepository.find({
      relations: ['questions'],
    });
  }

  findOne(id: number) {
    return this.formRepository.findOne({
      where: { form_id: id },
      relations: ['questions'],
    });
  }

  async update(id: number, updateFormDto: UpdateFormDto) {
    const form = await this.formRepository.findOne({
      where: { form_id: id },
    });
    if (!form) {
      throw new NotFoundException();
    }
    const updateForm = {
      ...form,
      ...updateFormDto,
    };
    return this.formRepository.save(updateForm);
  }

  async addQuestion(id: number, createQuestionDto: CreateQuestionDto) {
    const form = await this.formRepository.findOne({
      where: { form_id: id },
      relations: ['questions'],
    });
    if (!form) {
      throw new NotFoundException();
    }
    const newQuestion = await this.questionRepository.save(createQuestionDto);
    await this.questionRepository.save(newQuestion);
    form.questions.push(newQuestion);
    return this.formRepository.save(form);
  }

  async delQuestion(id: number) {
    const question = await this.questionRepository.findOne({
      where: { ques_id: id },
    });

    if (!question) {
      throw new NotFoundException();
    }
    return this.questionRepository.remove(question);
  }

  async remove(id: number) {
    const form = await this.formRepository.findOne({
      where: { form_id: id },
    });
    if (!form) {
      throw new NotFoundException();
    }
    return this.formRepository.remove(form);
  }
}
