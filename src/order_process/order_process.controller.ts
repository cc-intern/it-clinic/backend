import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { OrderProcessService } from './order_process.service';
import { CreateOrderProcessDto } from './dto/create-order_process.dto';
import { UpdateOrderProcessDto } from './dto/update-order_process.dto';

@Controller('order-process')
export class OrderProcessController {
  constructor(private readonly orderProcessService: OrderProcessService) {}

  @Post()
  create(@Body() createOrderProcessDto: CreateOrderProcessDto) {
    return this.orderProcessService.create(createOrderProcessDto);
  }

  @Get()
  findAll() {
    return this.orderProcessService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.orderProcessService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateOrderProcessDto: UpdateOrderProcessDto,
  ) {
    return this.orderProcessService.update(+id, updateOrderProcessDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.orderProcessService.remove(+id);
  }
}
