import { Module } from '@nestjs/common';
import { OrderProcessService } from './order_process.service';
import { OrderProcessController } from './order_process.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { OrderProcess } from './entities/order_process.entity';

@Module({
  imports: [TypeOrmModule.forFeature([OrderProcess])],
  controllers: [OrderProcessController],
  providers: [OrderProcessService],
})
export class OrderProcessModule {}
