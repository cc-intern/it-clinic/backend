import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { OrderRatingsService } from './order_ratings.service';
import { CreateOrderRatingDto } from './dto/create-order_rating.dto';
import { UpdateOrderRatingDto } from './dto/update-order_rating.dto';

@Controller('order-ratings')
export class OrderRatingsController {
  constructor(private readonly orderRatingsService: OrderRatingsService) {}

  @Post()
  create(@Body() createOrderRatingDto: CreateOrderRatingDto) {
    return this.orderRatingsService.create(createOrderRatingDto);
  }

  @Get()
  findAll() {
    return this.orderRatingsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.orderRatingsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateOrderRatingDto: UpdateOrderRatingDto,
  ) {
    return this.orderRatingsService.update(+id, updateOrderRatingDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.orderRatingsService.remove(+id);
  }
}
