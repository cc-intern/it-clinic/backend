import { Information } from 'src/informations/entities/information.entity';
import { Order } from 'src/orders/entities/order.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class OrderInfo {
  @PrimaryGeneratedColumn()
  order_info_id: number;

  @Column()
  order_info_answer: string;

  @Column()
  infoId: number;

  @Column()
  orderId: number;

  @ManyToOne(() => Information, (information) => information.order_info)
  infomation: Information;

  @ManyToOne(() => Order, (order) => order.order_info)
  order: Order;
}
