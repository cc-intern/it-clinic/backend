import { IsNotEmpty } from 'class-validator';
import { OrderInfo } from 'src/order_info/entities/order_info.entity';

export class CreateOrderDto {
  @IsNotEmpty()
  order_reserveDate: Date;

  @IsNotEmpty()
  order_username: string;

  @IsNotEmpty()
  orderInfo: OrderInfo[];
}
