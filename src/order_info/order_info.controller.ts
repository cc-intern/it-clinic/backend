import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { OrderInfoService } from './order_info.service';
import { CreateOrderInfoDto } from './dto/create-order_info.dto';
import { UpdateOrderInfoDto } from './dto/update-order_info.dto';

@Controller('order-info')
export class OrderInfoController {
  constructor(private readonly orderInfoService: OrderInfoService) {}

  @Post()
  create(@Body() createOrderInfoDto: CreateOrderInfoDto) {
    return this.orderInfoService.create(createOrderInfoDto);
  }

  @Get()
  findAll() {
    return this.orderInfoService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.orderInfoService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateOrderInfoDto: UpdateOrderInfoDto,
  ) {
    return this.orderInfoService.update(+id, updateOrderInfoDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.orderInfoService.remove(+id);
  }
}
