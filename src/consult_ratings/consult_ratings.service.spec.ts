import { Test, TestingModule } from '@nestjs/testing';
import { ConsultRatingsService } from './consult_ratings.service';

describe('ConsultRatingsService', () => {
  let service: ConsultRatingsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ConsultRatingsService],
    }).compile();

    service = module.get<ConsultRatingsService>(ConsultRatingsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
