import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import console from 'console';

import { Repository } from 'typeorm';
import { CreateConsultDto } from './dto/create-consult.dto';
import { UpdateConsultDto } from './dto/update-consult.dto';
import { Consult } from './entities/consult.entity';

@Injectable()
export class ConsultsService {
  constructor(
    @InjectRepository(Consult)
    private consultRepository: Repository<Consult>,
  ) {}
  create(createConsultDto: CreateConsultDto) {
    return this.consultRepository.save(createConsultDto);
  }

  findAll() {
    return this.consultRepository.find();
  }

  findTracking(username: string) {
    return this.consultRepository.find({
      where: {
        consult_username: username,
        consult_forms: [],
      },
    });
  }

  findOne(id: number) {
    return this.consultRepository.findOne({
      where: { consult_id: id },
    });
  }

  async update(id: number, updateConsultDto: UpdateConsultDto) {
    const consult = await this.consultRepository.findOne({
      where: { consult_id: id },
    });

    if (!consult) {
      throw new NotFoundException();
    }

    const updateConsult = {
      ...consult,
      ...updateConsultDto,
    };
    return this.consultRepository.save(updateConsult);
  }

  async remove(id: number) {
    const consult = await this.consultRepository.findOne({
      where: { consult_id: id },
    });

    if (!consult) {
      throw new NotFoundException();
    }
    return this.consultRepository.softRemove(consult);
  }
}
