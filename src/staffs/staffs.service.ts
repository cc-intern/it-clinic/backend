import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateStaffDto } from './dto/create-staff.dto';
import { UpdateStaffDto } from './dto/update-staff.dto';
import { Staff } from './entities/staff.entity';

@Injectable()
export class StaffsService {
  constructor(
    @InjectRepository(Staff)
    private staffRepository: Repository<Staff>,
  ) {}
  create(createStaffDto: CreateStaffDto) {
    return this.staffRepository.save(createStaffDto);
  }

  findAll() {
    return this.staffRepository.find();
  }

  findOne(id: number) {
    return this.staffRepository.findOne({
      where: { staff_id: id },
    });
  }

  async update(id: number, updateStaffDto: UpdateStaffDto) {
    const staff = await this.staffRepository.findOne({
      where: { staff_id: id },
    });
    if (!staff) {
      throw new NotFoundException();
    }
    const updateStaff = {
      ...staff,
      ...updateStaffDto,
    };
    return this.staffRepository.save(updateStaff);
  }

  async remove(id: number) {
    const staff = await this.staffRepository.findOne({
      where: { staff_id: id },
    });
    if (!staff) {
      throw new NotFoundException();
    }
    return this.staffRepository.remove(staff);
  }
}
