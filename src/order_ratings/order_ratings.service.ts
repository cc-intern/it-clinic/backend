import { Injectable } from '@nestjs/common';
import { CreateOrderRatingDto } from './dto/create-order_rating.dto';
import { UpdateOrderRatingDto } from './dto/update-order_rating.dto';

@Injectable()
export class OrderRatingsService {
  create(createOrderRatingDto: CreateOrderRatingDto) {
    return 'This action adds a new orderRating';
  }

  findAll() {
    return `This action returns all orderRatings`;
  }

  findOne(id: number) {
    return `This action returns a #${id} orderRating`;
  }

  update(id: number, updateOrderRatingDto: UpdateOrderRatingDto) {
    return `This action updates a #${id} orderRating`;
  }

  remove(id: number) {
    return `This action removes a #${id} orderRating`;
  }
}
