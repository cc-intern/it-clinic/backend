import { Form } from 'src/forms/entities/form.entity';
import { Order } from 'src/orders/entities/order.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class OrderForm {
  @PrimaryGeneratedColumn()
  order_form_id: number;

  @Column()
  order_form_status: string;

  @Column()
  orderId: number;

  @Column()
  formId: number;

  @ManyToOne(() => Order, (order) => order.order_forms)
  order: Order;

  @ManyToOne(() => Form, (form) => form.order_forms)
  form: Form;
}
