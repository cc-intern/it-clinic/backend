import { Module } from '@nestjs/common';
import { ConsultFormsService } from './consult_forms.service';
import { ConsultFormsController } from './consult_forms.controller';
import { ConsultForm } from './entities/consult_form.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([ConsultForm])],
  controllers: [ConsultFormsController],
  providers: [ConsultFormsService],
})
export class ConsultFormsModule {}
