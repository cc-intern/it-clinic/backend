import { Module } from '@nestjs/common';
import { InformationsService } from './informations.service';
import { InformationsController } from './informations.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Information } from './entities/information.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Information])],
  controllers: [InformationsController],
  providers: [InformationsService],
})
export class InformationsModule {}
