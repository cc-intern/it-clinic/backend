import { IsNotEmpty } from 'class-validator';

export class CreateOrderInfoDto {
  @IsNotEmpty()
  order_info_answer: string;
}
